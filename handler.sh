#!/bin/bash

key=$1

color1="\033[0;35m"
color1b="\033[1;35m"
color2="\033[0;34m"
color3="\033[0;32m"
color3b="\033[1;32m"
regular="\033[0m"

#echo -e "${color1}Registered ${color1b}${key}${color1}"
echo -e "${color1}Registered ${color1b}${key}${color1}"

MACROPATH="/home/dwl/lab/MacroKeyboard/macros"
DATAPATH="/home/dwl/lab/MacroKeyboard/data"

mode=$(cat $DATAPATH/mode.dat)
echo -e "Mode: $mode"

function setmode ()
{ 
	if [[ "$1" != "" ]]; then
		echo -e "${color3}Mode set to '${color3b}$1${color3}'..."
	fi
	echo $1 > $DATAPATH/mode.dat 
}

#echo "" > $DATAPATH/mode.dat
setmode ""

if [[ "$key" == "a" ]]; then
	source $MACROPATH/gitadd.sh 
elif [[ "$key" == "c" ]]; then
	source $MACROPATH/gitcommit.sh 
elif [[ "$key" == "i" ]]; then
	if [[ "$mode" == "labs-get" ]]; then
		source $MACROPATH/labsgetinstall.sh
	fi
elif [[ "$key" == "l" ]]; then
	if [[ "$mode" == "labs-get" ]]; then
		source $MACROPATH/labsgetlist.sh 
	else
		setmode "labs-get"
		source $MACROPATH/labsget.sh 
	fi
elif [[ "$key" == "p" ]]; then
	source $MACROPATH/pacmaninstall.sh
elif [[ "$key" == "s" ]]; then
	source $MACROPATH/gitstatus.sh
elif [[ "$key" == "u" ]]; then
	if [[ "$mode" == "labs-get" ]]; then
		source $MACROPATH/labsgetupdate.sh
	fi
elif [[ "$key" == "y" ]]; then
	source $MACROPATH/yaourtinstall.sh
elif [[ "$key" == "," ]]; then
	source $MACROPATH/gitpull.sh
elif [[ "$key" == "." ]]; then
	source $MACROPATH/gitpush.sh
elif [[ "$key" == "esc" ]]; then
	echo "Reseting mode..."
	setmode ""
fi

echo -e "\033[0m"
